// Copyright 2015 Two Bulls Holding Pty Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "vad.h"

#include <string>
#include <vector>
#include <iomanip>
#include <iostream>
#include <sstream>

#include <anyoption.h>
#include <tinyxml2.h>
#include <alljoyn/Init.h>

#include "BusBoy.h"

// Here we inherit from BusBoy because we specifically want to implement an Action handler for our "Press" Action.
//  If we didn't have any Actions to handle, then we could instead just create an instance of higgns::BusBoy directly.
class VirtualAllJoynDevice :
        public twobulls::BusBoy
{
public:
    VirtualAllJoynDevice(const std::string& aboutXML, const std::string& interfaceXML) :
            twobulls::BusBoy(aboutXML, interfaceXML, ajn::SESSION_PORT_ANY)
    {
        ExtractDeviceDetail(aboutXML);
        std::cout << "Created " << GetDeviceLabel() << std::endl;
    };

    void DoAction(const ajn::InterfaceDescription::Member* member, ajn::Message& message) {
        std::cout << GetDeviceLabel() << "." << member->name.c_str() << "(";

        size_t num_args = 0;
        const ajn::MsgArg* args = NULL;
        message->GetArgs(num_args, args);

        if (num_args > 0) {
            std::cout << std::endl;
        }

        for (size_t i = 0; i < num_args; ++i) {
            const ajn::MsgArg* arg = &args[i];
            std::cout << arg->ToString(4).c_str() << std::endl;
        }

        std::cout << ")" << std::endl;
    }

    bool GetProperty(const std::string &propertyName, ajn::MsgArg &outPropertyValue) {
        bool result = false;
        std::cout << GetDeviceLabel() << "." << propertyName << ".get { ";
        PropertyRegistry::iterator it = properties_.find(propertyName);
        if (it != properties_.end()) {
            outPropertyValue = it->second;
            std::cout <<  outPropertyValue.ToString(0);
            result = true;
        } else {
            std::cout << "<notfound>";
        }
        std::cout << " }" << std::endl;
        return result;
    }

    bool SetProperty(const std::string &propertyName, const ajn::MsgArg &inPropertyValue) {
        std::cout << GetDeviceLabel() << "." << propertyName << ".set { " << inPropertyValue.ToString(0) << " }" << std::endl;
        properties_[propertyName] = inPropertyValue;
        properties_[propertyName].Stabilize();
        return true;
    }

private:
    void ExtractDeviceDetail(const std::string &aboutXML) {
        tinyxml2::XMLDocument xmlDoc;

        device_name_.clear();
        device_id_.clear();

        if (xmlDoc.Parse(aboutXML.c_str()) != tinyxml2::XML_SUCCESS) {
            return;
        }

        const tinyxml2::XMLElement *root = xmlDoc.RootElement();
        for (const tinyxml2::XMLElement *child = root->FirstChildElement(); child != NULL; child = child->NextSiblingElement()) {
            const std::string childName(child->Name());
            if (childName == "DeviceId") {
                device_id_ = child->GetText();
            } else if (childName == "DeviceName") {
                device_name_ = child->GetText();
            }
        }
    }

    std::string GetDeviceLabel() const {
        return device_name_ + " ( " + device_id_ + " ) - " + GetInterfaceName();
    }

    typedef std::map< std::string, ajn::MsgArg > PropertyRegistry;
    PropertyRegistry properties_;
    std::string device_name_;
    std::string device_id_;
};

std::string readFile(const std::string &filePath) {
    std::stringstream buffer;
    std::ifstream file(filePath, ios::in);
    if (!file) {
        std::cout << "Error: " << filePath << " could not be read." << std::endl;
        return std::string();
    }
    buffer << file.rdbuf();
    return buffer.str();
}

static const char *defaultBusConfig =
    "<busconfig>"
    "  <type>alljoyn_bundled</type>"
    "  <listen>tcp:iface=*,port=0</listen>"
    "  <listen>udp:iface=*,port=0</listen>"
    "  <limit name=\"auth_timeout\">20000</limit>"
    "  <limit name=\"max_incomplete_connections\">4</limit>"
    "  <limit name=\"max_completed_connections\">16</limit>"
    "  <limit name=\"max_remote_clients_tcp\">64</limit>"
    "  <limit name=\"max_remote_clients_udp\">64</limit>"
    "  <property name=\"router_power_source\">Battery powered and chargeable</property>"
    "  <property name=\"router_mobility\">Intermediate mobility</property>"
    "  <property name=\"router_availability\">3-6 hr</property>"
    "  <property name=\"router_node_connection\">Wireless</property>"
    "</busconfig>";

int main(int argc, char** argv)
{
    AnyOption opt;

    opt.addUsage("Usage:");
    opt.addUsage(" -h --help                        Print this help");
    opt.addUsage(" -a --about about.xml             About xml file path");
    opt.addUsage(" -i --interface interface.xml     Interface xml file path");
    opt.addUsage(" -b --busconfig busconfig.xml     BusConfig xml file path");
    opt.addUsage(" -c --count X                     Number of devices to create");

    opt.setFlag("help", 'h');
    opt.setOption("about", 'a');
    opt.setOption("interface", 'i');
    opt.setOption("busconfig", 'b');
    opt.setOption("count", 'c');

    opt.processCommandArgs(argc, argv);

    if (!opt.hasOptions() || opt.getValue('a') == NULL || opt.getValue('i') == NULL) {
        opt.printUsage();
        return 1;
    }

    const std::string aboutXML = readFile(opt.getValue('a'));
    const std::string interfaceXML = readFile(opt.getValue('i'));
    const std::string busconfigXML = readFile(opt.getValue('b'));

    if (aboutXML.length() < 1 || interfaceXML.length() < 1) {
        opt.printUsage();
        return 1;
    }

    if (busconfigXML.length() < 1) {
        std::cout << "! Using default busconfig (all network interfaces) !" << std::endl;
        AllJoynRouterSetConfig(defaultBusConfig);
    } else {
        AllJoynRouterSetConfig(busconfigXML.c_str());
    }

    std::stringstream converter;

    int count = 1;
    const char* countArg = opt.getValue('c');
    if (countArg != NULL) {
        converter << countArg;
        converter >> count;
    }

    std::vector<VirtualAllJoynDevice*> devices;
    if (count > 1) {
        const std::string deviceIdOpen("<DeviceId>");
        const std::string deviceNameClose("</DeviceName>");
        const int deviceIdFirstPartLength = 8;

        for (int i = 0; i < count; ++i) {
            std::string numberedAboutXML = aboutXML;
            std::stringstream numberConverter; numberConverter << std::setfill('0') << std::setw(deviceIdFirstPartLength) << i;
            const std::string number = numberConverter.str();
            // we insert 0000000X at the end of the DeviceName entry
            numberedAboutXML.insert(numberedAboutXML.find(deviceNameClose), number);
            // we replace the first 8 characters of the DeviceId entry with 0000000X
            numberedAboutXML.replace(numberedAboutXML.find(deviceIdOpen) + deviceIdOpen.length(), deviceIdFirstPartLength, number);
            devices.push_back(new VirtualAllJoynDevice(numberedAboutXML, interfaceXML));
        }
    } else {
        devices.push_back(new VirtualAllJoynDevice(aboutXML, interfaceXML));
    }

    for (auto& device : devices) {
        device->Start();
    }

    std::cout << std::endl << "CTRL+C/BREAK to exit." << std::endl;
    while (std::cin.ignore()) {
        // Swallow input until the user CTRL+C/BREAK
    }

    for (auto& device : devices) {
        device->Stop();
        delete device;
        device = NULL;
    }

    devices.clear();

    return 0;
}