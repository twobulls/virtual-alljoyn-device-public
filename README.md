VirtualAllJoynDevice
====================

Dependencies
============

* CMake 3.3+
* SCons 2.3.2+ (for alljoyn)
* Xcode (6+) (for darwin)

Usage
=====

    git submodule update --init --recursive
    mkdir -p build/darwin
    cd build/darwin
    cmake -DPROJECT_PLATFORM=darwin ../../
    make alljoyn_darwin
    make virtual-alljoyn-device
    ./virtual-alljoyn-device -h (and follow instructions)

Example
=======

* about.xml

        <About>
            <AppId>f39cae6e-72d9-437b-a1de-989fa73c2f9b</AppId>
            <DeviceId>abcd1234-72d9-437b-a1de-989fa73c2f9b</DeviceId>
            <AppName>VirtualAllJoynDevice</AppName>
            <Manufacturer>Two Bulls</Manufacturer>
            <ModelNumber>001</ModelNumber>
            <Description>A device that can receive a message</Description>
            <SoftwareVersion>0.0.1</SoftwareVersion>
            <DefaultLanguage>en</DefaultLanguage>
            <DeviceName>ExampleDevice</DeviceName>
        </About>

* interface.xml

        <interface name="com.twobulls.busboy.example">
            <description language="en">This is an example interface</description>
        
            <property name="LastMessage" type="s" access="readwrite">
                <description>This is an example Property</description>
            </property>
        
            <signal name="MessageSaid" sessionless="true">
                <description>This is an example Event</description>
            </signal>
        
            <method name="SayMessage">
                <description>This is an example Action</description>
                <arg name="messageArg" type="s" direction="in">
                    <description>This is an example Action string argument</description>
                </arg>
            </method>
        </interface>

* busconfig.xml

        <busconfig>
            <type>alljoyn_bundled</type>
            <listen>tcp:iface=*,port=0</listen>
            <listen>udp:iface=*,port=0</listen>
            <limit name=\"auth_timeout\">20000</limit>
            <limit name=\"max_incomplete_connections\">4</limit>
            <limit name=\"max_completed_connections\">16</limit>
            <limit name=\"max_remote_clients_tcp\">64</limit>
            <limit name=\"max_remote_clients_udp\">64</limit>
            <property name=\"router_power_source\">Battery powered and chargeable</property>
            <property name=\"router_mobility\">Intermediate mobility</property>
            <property name=\"router_availability\">3-6 hr</property>
            <property name=\"router_node_connection\">Wireless</property>
        </busconfig>

Create 5 different instances of the device with the same alljoyn interface on all (iface=*) network interfaces

        ./virtual-alljoyn-device -a about.xml -i interface.xml -b busconfig.xml -c 5
        
Reference
=========

interface guidelines: https://wiki.allseenalliance.org/irb/description
arg types: https://dbus.freedesktop.org/doc/dbus-specification.html#idp94392448
busconfig: https://allseenalliance.org/framework/documentation/learn/core/rn_config
