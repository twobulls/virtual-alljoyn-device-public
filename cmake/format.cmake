file(GLOB_RECURSE PROJECT_ALL_PLATFORM_SRC
	platform/*/src/*.cpp
	platform/*/src/*.c
	platform/*/src/*.h
)

file(GLOB_RECURSE PROJECT_ALL_DEMO_APP_SRC
	demo_apps/*/*.cpp
	demo_apps/*/*.c
	demo_apps/*/*.h
)

set(CLANG_FORMAT "NOTSET" CACHE STRING "Path to the clang-format executable")



if("${CLANG_FORMAT}" STREQUAL "NOTSET")
	find_program(FIND_CLANG_FORMAT clang-format)
	if("${FIND_CLANG_FORMAT}" STREQUAL "CLANG_FORMAT-NOTFOUND")
		message(FATAL_ERROR "Could not find 'clang-format' please set CLANG_FORMAT:STRING")
	else()
		set(CLANG_FORMAT ${FIND_CLANG_FORMAT})
		message(STATUS "Found: ${CLANG_FORMAT}")
	endif()
endif()


if(EXISTS ${CLANG_FORMAT})
	add_custom_target(format 
		COMMAND ${CLANG_FORMAT} -style=file -fallback-style=none -i ${PROJECT_SRC} ${PROJECT_ALL_PLATFORM_SRC} ${PROJECT_ALL_DEMO_APP_SRC}
		COMMENT "Reformatting all your source code!"
	)

	# If there are <replacment tags in the output xml, then the source code isn't clean, and we "fail" the --build <dir>
	add_custom_target(format_check
		COMMAND ! ${CLANG_FORMAT} -style=file -fallback-style=none -output-replacements-xml ${PROJECT_SRC} ${PROJECT_ALL_PLATFORM_SRC} ${PROJECT_ALL_DEMO_APP_SRC} | grep -c '^<replacement '
		COMMENT "Checking the formatting of source files..."
	)	
endif()


