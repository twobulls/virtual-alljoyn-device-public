# This file setups up CMake to cross compile for ARM based OpenWRT 
# Please make sure STAGING_DIR is set and stays set


SET(CMAKE_SYSTEM_NAME Linux)

# Our own environment variable for detecting 
SET(PROJECT_PLATFORM openwrt)
include(CMakeForceCompiler)

# Setup OPENWRT paths, some of these are used in openwrt.cmake 
set(STAGING_DIR "$ENV{STAGING_DIR}")
set(OPENWRT_TOOLCHAIN_SUFFIX arm_cortex-a9+vfpv3_gcc-4.8-linaro_uClibc-0.9.33.2_eabi)
set(OPENWRT_TARGET_DIR ${STAGING_DIR}/target-arm_cortex-a9+vfpv3_uClibc-0.9.33.2_eabi)

# Where to find the cross compiler
set(CROSS_COMPILER_PREFIX ${STAGING_DIR}/toolchain-${OPENWRT_TOOLCHAIN_SUFFIX}/bin/arm-openwrt-linux-)
set(CMAKE_C_COMPILER   ${CROSS_COMPILER_PREFIX}gcc)
set(CMAKE_CXX_COMPILER ${CROSS_COMPILER_PREFIX}g++)
	
