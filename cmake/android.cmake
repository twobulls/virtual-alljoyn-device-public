message(STATUS "Setting up for a Android build")

# Files used only by Android
file(GLOB PROJECT_PLATFORM_SRC
	platform/android/src/*.cpp
	platform/android/src/*.h
)

# Include paths
include_directories(
	platform/android/src
)

get_filename_component(PROJECT_PLATFORM_EXPORT_INCLUDES "platform/android/src" ABSOLUTE)
set(PROJECT_EXPORT_INCLUDES 
	${PROJECT_EXPORT_INCLUDES}
	${PROJECT_PLATFORM_EXPORT_INCLUDES}
)

# generated from alljoyn Android.mk which invokes SCons build
set(PROJECT_ALLJOYN_LIBS
	alljoyn_notification
	alljoyn_config
	alljoyn_services_common
	alljoyn_onboarding
	alljoyn_about
	ajrouter
	alljoyn
	alljoyn_common_static
)

# Preprocessor defines
set_property(DIRECTORY APPEND PROPERTY COMPILE_DEFINITIONS
	PROJECT_PLATFORM_ANDROID

	# Alljoyn setup
	QCC_OS_GROUP_POSIX
	QCC_OS_ANDROID
	ALLJOYN_VERSION=1509
	ALLJOYN_BUNDLED_ROUTER

	HAVE_POSIX_FALLOCATE=0
	BOOST_LEXICAL_CAST_ASSUME_C_LOCALE=1

	PROJECT_ENABLE_LOGGING
)

# Add common (C/C++) compiler flags
add_compile_options(
	# Code Generation options
	-std=gnu++0x
	-fPIC
	-Wall
	-Werror=non-virtual-dtor
	-pipe
	-fno-strict-aliasing
	-Wno-deprecated
	-fno-rtti
	-fexceptions
)

set(ALLJOYN_PLATFORM_ENV
	CONFIGURATION=$<$<CONFIG:Debug>:debug>$<$<NOT:$<CONFIG:Debug>>:release>
	JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Versions/1.6
	CLASSPATH=/not_actually_used_for_our_builds_but_must_be_defined.meh
)

set(ALLJOYN_ROOT_DIR "${PROJECT_SOURCE_DIR}/external/alljoyn/core/alljoyn")

# note that TARGET_ARCH and BUILD_TYPE is determined at build time by the NDK
set(ALLJOYN_DIST_DIR "${ALLJOYN_ROOT_DIR}/build/android/\$(TARGET_ARCH)/\$(BUILD_TYPE)/dist")

set(ALLJOYN_CORE_EXPORT_C_INCLUDES
	${ALLJOYN_DIST_DIR}/cpp/inc
	${ALLJOYN_DIST_DIR}/cpp/inc/alljoyn # FIXME: Status.h
	${ALLJOYN_ROOT_DIR}/common/inc
	${ALLJOYN_ROOT_DIR}/alljoyn_core/src
	${ALLJOYN_DIST_DIR}/onboarding/inc
)

foreach(PROJECT_PLATFORM_ABI arm x86)
	set(ALLJOYN_PLATFORM_CONFIG
		SERVICES=config,notification,onboarding
		BINDINGS=cpp
		OS=android
		CPU=${PROJECT_PLATFORM_ABI}
		CRYPTO=builtin
		ANDROID_NDK=$ENV{ANDROID_NDK_HOME}
		ANDROID_API_LEVEL=18
	)

	add_custom_target(alljoyn_${PROJECT_PLATFORM}_${PROJECT_PLATFORM_ABI}
		COMMAND ${CMAKE_COMMAND}
			-DALLJOYN_ENV="${ALLJOYN_PLATFORM_ENV}"
			-DALLJOYN_CONFIG="${ALLJOYN_PLATFORM_CONFIG}"
			-DALLJOYN_DIR="${ALLJOYN_ROOT_DIR}"
			-P ${PROJECT_SOURCE_DIR}/cmake/alljoyn_scons.cmake
	)
	add_dependencies(alljoyn_${PROJECT_PLATFORM} alljoyn_${PROJECT_PLATFORM}_${PROJECT_PLATFORM_ABI})

	add_custom_target(clean-alljoyn_${PROJECT_PLATFORM}_${PROJECT_PLATFORM_ABI}
		COMMAND ${CMAKE_COMMAND} 
			-DALLJOYN_ENV="${ALLJOYN_PLATFORM_ENV}"
			-DALLJOYN_CONFIG="${ALLJOYN_PLATFORM_CONFIG};--clean"
			-DALLJOYN_DIR="${ALLJOYN_ROOT_DIR}"
			-P ${PROJECT_SOURCE_DIR}/cmake/alljoyn_scons.cmake
	)
	add_dependencies(clean-alljoyn_${PROJECT_PLATFORM} clean-alljoyn_${PROJECT_PLATFORM}_${PROJECT_PLATFORM_ABI})
endforeach(PROJECT_PLATFORM_ABI)

# we collate COMPILE_DEFINITIONS by prepending each entry with a -D
get_property(PROJECT_COMPILE_DEFINITIONS DIRECTORY PROPERTY COMPILE_DEFINITIONS)
string(REGEX REPLACE "([a-zA-Z0-9_=/-]+)(;|$)" "-D\\1 " PROJECT_COMPILE_DEFINITIONS "${PROJECT_COMPILE_DEFINITIONS}")
get_property(PROJECT_COMPILE_OPTIONS DIRECTORY PROPERTY COMPILE_OPTIONS)
# we prepare cppflags by stripping out any ; characters and any cmake generator statements
string(REGEX REPLACE "\\$<\\$.*>:.*>|;" " " PROJECT_ANDROID_CPPFLAGS "${PROJECT_COMPILE_DEFINITIONS} ${PROJECT_COMPILE_OPTIONS}")

# we prepare android includes by stripping out any ; characters
get_property(PROJECT_ANDROID_INCLUDES DIRECTORY PROPERTY INCLUDE_DIRECTORIES)
string(REPLACE ";" " " PROJECT_ANDROID_INCLUDES "${PROJECT_ANDROID_INCLUDES}")

# we prepare android sources by stripping out any ; characters and anything that isnt a .c or .cpp file
string(REGEX MATCHALL "([a-zA-Z0-9_/-]+.c[pp]*)(;|$)" PROJECT_ANDROID_SOURCES "${PROJECT_SRC} ${PROJECT_SRC_PLATFORM} ${EXTERNAL_SRC} ${MOCK_SRC}")
string(REPLACE ";" " " PROJECT_ANDROID_SOURCES "${PROJECT_ANDROID_SOURCES}")

# we prepare android exports by stripping out any ; characters
string(REPLACE ";" " " PROJECT_ANDROID_EXPORT_INCLUDES "${PROJECT_EXPORT_INCLUDES}")

# we prepare android libs by stripping out any ; characters
string(REPLACE ";" " " PROJECT_ANDROID_LIBS "${PROJECT_ALLJOYN_LIBS}")

# we prepare alljoyn includes by stripping out any ; characters
string(REPLACE ";" " " ALLJOYN_CORE_EXPORT_C_INCLUDES "${ALLJOYN_CORE_EXPORT_C_INCLUDES}")

# we set a handy dandy reminder for users of the output
string(TIMESTAMP PROJECT_ANDROID_MAKE_TIME)
set(PROJECT_ANDROID_NOTES "================================================================================================== \\\n THIS FILE WAS MACHINE GENERATED FROM CMAKE AT ${PROJECT_ANDROID_MAKE_TIME} \\\n ANY IN PLACE EDITS MAY BE DISCARDED - DO NOT COMMIT THIS FILE TO REPO \\\n ANY REQUIRED CHANGES SHOULD BE MADE TO Android.mk.in AND THEN COMMITTED \\\n================================================================================================== \\\n")

# here we manufacture the Android.mk from our template
#configure_file(${PROJECT_SOURCE_DIR}/platform/android/Project/app/src/main/jni/project-core/Android.mk.in ${PROJECT_SOURCE_DIR}/platform/android/Project/app/src/main/jni/project-core/Android.mk @ONLY)

#configure_file(${PROJECT_SOURCE_DIR}/platform/android/Project/app/src/main/jni/alljoyn/Android.mk.in ${PROJECT_SOURCE_DIR}/platform/android/Project/app/src/main/jni/alljoyn/Android.mk @ONLY)
