separate_arguments(ALLJOYN_ENV)
foreach(ENV_PAIR ${ALLJOYN_ENV})
	string(REPLACE "=" ";" ENV_PAIR_LIST ${ENV_PAIR})
	list(GET ENV_PAIR_LIST 0 ENV_KEY)
	list(GET ENV_PAIR_LIST 1 ENV_VAL)
	message("Setting env[${ENV_KEY}] = ${ENV_VAL}")
	set(ENV{${ENV_KEY}} ${ENV_VAL})
endforeach(ENV_PAIR)

separate_arguments(ALLJOYN_CONFIG)
list(APPEND ALLJOYN_CONFIG
	VARIANT=$ENV{CONFIGURATION}
	BUILD_SERVICES_SAMPLES=off 
	BR=on 
	WS=off 
	V=1 
	-j 10
)

message("Path is: $ENV{PATH}")

message("Building... scons ${ALLJOYN_CONFIG}\n")

execute_process(COMMAND scons ${ALLJOYN_CONFIG} 
	WORKING_DIRECTORY "${ALLJOYN_DIR}"
	OUTPUT_VARIABLE scons_output
	ERROR_VARIABLE scons_output
	RESULT_VARIABLE scons_result
)

message("${scons_result}\n${scons_output}\n")

if(scons_result)
	message(FATAL_ERROR "*** scons failed: ${scons_result}")
endif()
