message(STATUS "Setting up for a OpenWRT build")
message(STATUS "C++ compiler: ${CMAKE_CXX_COMPILER}")

# Files used only by OpenWRT
file(GLOB_RECURSE PROJECT_PLATFORM_SRC
	platform/openwrt/src/*.cpp
	platform/openwrt/src/*.h
)


# Include paths
include_directories(
	# System include
	"${OPENWRT_TARGET_DIR}/usr/include"
#	"${OPENWRT_TARGET_DIR}/usr/include/uClibc++"

	platform/openwrt/src
)


# Linker flags
set(PROJECT_LDFLAGS
	-static-libstdc++
	# System libraries
    pthread
    gcc
    c
    m
    crypto
    ssl
    dl

		-L${OPENWRT_TARGET_DIR}/usr/lib
    # Alljoyn
    alljoyn
    alljoyn_about
    alljoyn_services_common
    alljoyn_notification

)

# Preprocessor defines
set_property(DIRECTORY APPEND PROPERTY COMPILE_DEFINITIONS
	PLATFORM_OPENWRT

	# Alljoyn setup
	QCC_OS_GROUP_POSIX
	QCC_OS_LINUX
	ALLJOYN_VERSION=1504


)

# Add common (C/C++) compiler flags
add_compile_options(
	# Code Generation options
	$<$<CONFIG:Debug>:-O0> # No optimization in debug builds
	$<$<NOT:$<CONFIG:Debug>>:-Os> # Non-Debug optimization
	-g3
	-pthread
	-fno-rtti
	-std=gnu++11
)

# include executable targets
include(cmake/executable.cmake)