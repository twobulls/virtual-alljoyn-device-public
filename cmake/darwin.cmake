MESSAGE(STATUS "Setting up for a Darwin build")

set(DARWIN_DEV_ROOT ${OSX_DEVELOPER_ROOT})
set(DARWIN_SDK_ROOTS ${DARWIN_DEV_ROOT}/Platforms/MacOSX.platform/Developer/SDKs)

file(GLOB DARWIN_SDKS
	${DARWIN_SDK_ROOTS}/MacOSX*.sdk
)

string(REGEX REPLACE "${DARWIN_SDK_ROOTS}/MacOSX([0-9]+)\.[0-9]+\.sdk(;|$)" "\\1;" DARWIN_SDK_VERSION_MAJOR "${DARWIN_SDKS}")
string(REGEX REPLACE "${DARWIN_SDK_ROOTS}/MacOSX[0-9]+\.([0-9]+)\.sdk(;|$)" "\\1;" DARWIN_SDK_VERSION_MINOR "${DARWIN_SDKS}")

set(DARWIN_SDK_VERSION_VALUES "")
list(LENGTH DARWIN_SDKS DARWIN_SDK_COUNT)
math(EXPR DARWIN_SDK_COUNT "${DARWIN_SDK_COUNT} - 1")
set(NEWEST_SDK_VERSION_INDEX "")
set(NEWEST_SDK_VERSION_VALUE 0)
foreach(INDEX RANGE ${DARWIN_SDK_COUNT})
	list(GET DARWIN_SDK_VERSION_MAJOR ${INDEX} MAJOR)
	list(GET DARWIN_SDK_VERSION_MINOR ${INDEX} MINOR)
	math(EXPR DARWIN_SDK_VERSION_VALUE "${MAJOR} * 100000 + ${MINOR}")
	if(${DARWIN_SDK_VERSION_VALUE} GREATER ${NEWEST_SDK_VERSION_VALUE})
		set(NEWEST_SDK_VERSION_INDEX ${INDEX})
		set(NEWEST_SDK_VERSION_VALUE ${DARWIN_SDK_VERSION_VALUE})
	endif()
endforeach()
list(GET DARWIN_SDKS ${NEWEST_SDK_VERSION_INDEX} NEWEST_DARWIN_SDK)
set(DARWIN_SDK_ROOT ${NEWEST_DARWIN_SDK})

MESSAGE(STATUS "Newest Darwin SDK identified at ${DARWIN_SDK_ROOT}")

# Use a "generator expression" to generate the alljoyn directory name based on the config (release/debug)
# (end result is [...]/x86/debug/dist or [...]/x86/release/dist)
set(ALLJOYN_PATH "${PROJECT_SOURCE_DIR}/external/alljoyn/core/alljoyn/build/darwin/x86/$<$<CONFIG:Debug>:debug>$<$<NOT:$<CONFIG:Debug>>:release>/dist")

set(ALLJOYN_PLATFORM_ENV
	CONFIGURATION=$<$<CONFIG:Debug>:debug>$<$<NOT:$<CONFIG:Debug>>:release>
)

set(ALLJOYN_PLATFORM_CONFIG
	SDKROOT=${DARWIN_SDK_ROOT}
	SYSTEM_DEVELOPER_DIR=${DARWIN_DEV_ROOT}
	SERVICES=notification,onboarding
	BINDINGS=cpp
	CPU=x86
	CRYPTO=builtin
)

add_custom_command(TARGET alljoyn_${PROJECT_PLATFORM}
	PRE_BUILD
	COMMAND ${CMAKE_COMMAND}
		-DALLJOYN_ENV="${ALLJOYN_PLATFORM_ENV}"
		-DALLJOYN_CONFIG="${ALLJOYN_PLATFORM_CONFIG}"
		-DALLJOYN_DIR="${PROJECT_SOURCE_DIR}/external/alljoyn/core/alljoyn"
		-P ${PROJECT_SOURCE_DIR}/cmake/alljoyn_scons.cmake
)

add_custom_command(TARGET clean-alljoyn_${PROJECT_PLATFORM}
	PRE_BUILD
	COMMAND ${CMAKE_COMMAND} 
		-DALLJOYN_ENV="${ALLJOYN_PLATFORM_ENV}"
		-DALLJOYN_CONFIG="${ALLJOYN_PLATFORM_CONFIG};--clean"
		-DALLJOYN_DIR="${PROJECT_SOURCE_DIR}/external/alljoyn/core/alljoyn"
		-P ${PROJECT_SOURCE_DIR}/cmake/alljoyn_scons.cmake
)

# Files used only by darwin
file(GLOB_RECURSE PROJECT_SRC_PLATFORM
	platform/darwin/src/*.cpp
	platform/darwin/src/*.h
)

# Include paths
include_directories(
	# Alljoyn include paths
	SYSTEM ${ALLJOYN_PATH}/cpp/inc
	SYSTEM ${ALLJOYN_PATH}/cpp/inc/alljoyn # FIXME: Status.h
	SYSTEM ${ALLJOYN_PATH}/notification/inc
	SYSTEM ${ALLJOYN_PATH}/services_common/inc
	SYSTEM ${ALLJOYN_PATH}/onboarding/inc	
	SYSTEM ${PROJECT_SOURCE_DIR}/external/alljoyn/core/alljoyn/common/inc
	SYSTEM ${PROJECT_SOURCE_DIR}/external/alljoyn/core/alljoyn/alljoyn_core/src
)

# Linker flags
set(PROJECT_LDFLAGS
	# System
    crypto

    # Alljoyn libraries
    -L${ALLJOYN_PATH}/cpp/lib
    alljoyn
    ajrouter
    alljoyn_about

	-L${ALLJOYN_PATH}/services_common/lib
    alljoyn_services_common

	-L${ALLJOYN_PATH}/notification/lib
    alljoyn_notification

    -L${ALLJOYN_PATH}/onboarding/lib
    alljoyn_onboarding
)


# Preprocessor defines
set_property(DIRECTORY APPEND PROPERTY COMPILE_DEFINITIONS
	PROJECT_PLATFORM_DARWIN

	#Alljoyn setup
	QCC_OS_GROUP_POSIX
	QCC_OS_DARWIN
	QCC_CPU_X86
	ALLJOYN_BUNDLED_ROUTER
	ALLJOYN_VERSION=1509
	MECHANISM_PIPE
)

# Add common (C/C++) compiler flags
add_compile_options(
	# Code Generation options
	$<$<CONFIG:Debug>:-O0> # No optimization in debug builds
	$<$<NOT:$<CONFIG:Debug>>:-Os> # Non-Debug optimization
	-arch x86_64
	-g
	-fmessage-length=0
	-fdiagnostics-show-note-include-stack
	-fmacro-backtrace-limit=0
	-stdlib=libc++
	-mmacosx-version-min=10.9
	-fno-rtti
	-fpascal-strings
	-fasm-blocks
	-fstrict-aliasing
	-fvisibility-inlines-hidden
	-fno-common

	# Warning options
	-Wall
	-Werror
)

# C++ specific compiler flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11")

# include executable targets
include(cmake/executable.cmake)
