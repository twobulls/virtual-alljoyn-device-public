####################
# executable targets
####################

########################
# virtual-alljoyn-device
########################
add_executable(virtual-alljoyn-device ${PROJECT_SRC} ${PROJECT_SRC_PLATFORM} ${EXTERNAL_SRC})
#target_compile_definitions(virtual-alljoyn-device PUBLIC ENABLE_BUSBOY_LOGGING)
target_link_libraries(virtual-alljoyn-device ${PROJECT_LDFLAGS})
